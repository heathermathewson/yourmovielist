//
//  AddNewMovieVC.h
//  MovieWatch
//
//  Created by Class on 10/29/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewMovieVC : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *buttonDropDown;
@property (weak, nonatomic) IBOutlet UITextField *textFieldMovieTitle;
@property (weak, nonatomic) IBOutlet UITextField *textFieldNewMovieGenre;
@property (weak, nonatomic) IBOutlet UITableView *tableViewGenres;
@property (weak, nonatomic) IBOutlet UIView *viewHiding;

@end
