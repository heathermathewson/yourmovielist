//
//  AddNewMovieVC.m
//  MovieWatch
//
//  Created by Class on 10/29/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import "AddNewMovieVC.h"
#import "Movies.h"
#import <Parse/Parse.h>
#import <Google/Analytics.h>


@interface AddNewMovieVC ()
{
    NSArray *arrayOfGenres;
    
}

@end

@implementation AddNewMovieVC

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayOfGenres = [[NSArray alloc]initWithObjects:@"Drama",@"Sci-Fi", @"Action", @"Comedy", @"Romantic Comedy", @"Horror", @"Fantasy", @"Documentary", @"Kids", @"Noir", @"Musical", nil];
    
    [self setButtonToHide];
}

-(void)viewWillAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Adding New Movie"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (IBAction)pressedButtonSaveMovie:(id)sender
{
    PFUser *newUser = [PFUser currentUser];
    PFObject *addNewMovie = [PFObject objectWithClassName:@"MovieList"];
    [newUser addObject:addNewMovie forKey:@"UsersMovieList"];
    
    addNewMovie[@"movieName"] = self.textFieldMovieTitle.text;
    addNewMovie[@"movieGenre"] = self.textFieldNewMovieGenre.text;
    [addNewMovie saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            NSLog(@"Movie info logged to parse");
        }
        else
        {
            NSLog(@"Error:Movie did not log to parse");
        }
    }];
    
    
    [newUser saveInBackground];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"frequency" object:[Movies movieName:self.textFieldMovieTitle.text withGenre:self.textFieldNewMovieGenre.text withObjectId:addNewMovie.objectId]];
    
    self.textFieldMovieTitle.text = @"";
    self.textFieldNewMovieGenre.text = @"";
    
    [self.textFieldMovieTitle becomeFirstResponder];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.textFieldMovieTitle)
    {
        [self.textFieldNewMovieGenre becomeFirstResponder];
    }
    else if (textField == self.textFieldNewMovieGenre)
    {
        [self pressedButtonSaveMovie:nil];
    }
    
    return YES;
}
- (IBAction)pressedShowGenres:(UIButton *)sender
{
    if (sender.tag == 0)
    {
        [self setButtonToHide];
    }
    else
    {
        [self setButtonToDisplay];
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSString *usersGenre = [arrayOfGenres objectAtIndex:indexPath.row];
    cell.textLabel.text = usersGenre;
    cell.backgroundColor = [UIColor lightGrayColor];
    
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayOfGenres count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selectedGenreString = [arrayOfGenres objectAtIndex:indexPath.row];
    self.textFieldNewMovieGenre.text = selectedGenreString;
    //    self.viewHiding.hidden = YES;
    [self pressedShowGenres: nil];
    
    [self setButtonToHide];
}


-(void)setButtonToDisplay
{
    [self.buttonDropDown setTitle:@"▲" forState:UIControlStateNormal];
    self.viewHiding.hidden = YES;
    self.buttonDropDown.tag = 0;
}

-(void)setButtonToHide
{
    self.buttonDropDown.tag = 1;
    [self.buttonDropDown setTitle:@"▼" forState:UIControlStateNormal];
    self.viewHiding.hidden = NO;
    
    
}

@end
