//
//  Movies.h
//  MovieWatch
//
//  Created by Class on 10/30/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movies : NSObject

@property (strong, nonatomic)NSString *movieNameString;
@property (strong, nonatomic)NSString *movieGenreString;
@property (strong, nonatomic)NSString *objectIdString;

@property(strong, nonatomic)NSString *usersGenreString;

+(Movies *)movieName:(NSString *)movieNameToSet withGenre:(NSString *)movieGenreToSet withObjectId:(NSString *)objectId;

@end
