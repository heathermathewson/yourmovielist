//
//  Movies.m
//  MovieWatch
//
//  Created by Class on 10/30/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import "Movies.h"

@implementation Movies

+(Movies *)movieName:(NSString *)movieNameToSet withGenre:(NSString *)movieGenreToSet withObjectId:(NSString *)objectId
{
    Movies *newMovie = [[Movies alloc]init];
    newMovie.movieNameString = movieNameToSet;
    newMovie.movieGenreString = movieGenreToSet;
    newMovie.objectIdString = objectId;
    
    return newMovie;

}

@end
