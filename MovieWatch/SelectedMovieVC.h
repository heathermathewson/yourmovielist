//
//  SelectedMovieVC.h
//  MovieWatch
//
//  Created by Class on 10/29/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movies.h"



@interface SelectedMovieVC : UIViewController

@property (strong, nonatomic)Movies *movieSelected;
@property (weak, nonatomic) IBOutlet UILabel *labelMovieTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageSelectedMovie;
@property (weak, nonatomic) IBOutlet UITextView *labelSelectedMovieInfo;

@property (weak, nonatomic) IBOutlet UIImageView *imagetmdb;

@end
