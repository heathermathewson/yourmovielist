//
//  SelectedMovieVC.m
//  MovieWatch
//
//  Created by Class on 10/29/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import "SelectedMovieVC.h"
#import <Parse/Parse.h>
#import <Google/Analytics.h>
#import "TMDBAPI.h"
#import <JLTMDbClient.h>
#import <UIImageView+AFNetworking.h>
#import "WebViewVC.h"



@interface SelectedMovieVC ()

@end

@implementation SelectedMovieVC

-(void)viewWillAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Selected Movie"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.labelMovieTitle.text = self.movieSelected.movieNameString;
    TMDBAPI *recievedMovies = [[TMDBAPI alloc]init];
    [recievedMovies gettingMovies];
    __block NSString *imageBackdrop;
    __block UIAlertView *errorAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"") message:NSLocalizedString(@"Please try again later", @"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", @""), nil];
    
    [[JLTMDbClient sharedAPIInstance] GET:kJLTMDbSearchMovie withParameters:@{@"query":self.movieSelected.movieNameString}
     andResponseBlock:^(id response, NSError *error) {
//    [[JLTMDbClient sharedAPIInstance] GET:kJLTMDbMovie withParameters:@{@"id":@131631} andResponseBlock:^(id response, NSError *error) {
         if (!error && [response[@"results"] count] > 1) {
            response = response[@"results"][0];
             [self.imagetmdb setImage:[UIImage imageNamed:@"tmdb.png"]];
            self.labelSelectedMovieInfo.text = response[@"overview"];
            if (response[@"backdrop_path"] != [NSNull null]){
//                imageBackdrop = [self.imagesBaseUrlString stringByReplacingOccurrencesOfString:@"w92" withString:@"w780"];
                [self.imageSelectedMovie setImageWithURL:[NSURL URLWithString:[@"http://image.tmdb.org/t/p/w780" stringByAppendingString:response[@"backdrop_path"]]]];
            } else {
//                imageBackdrop = [self.imagesBaseUrlString stringByReplacingOccurrencesOfString:@"w92" withString:@"w500"];
                [self.imageSelectedMovie setImageWithURL:[NSURL URLWithString:[@"http://image.tmdb.org/t/p/w500" stringByAppendingString:response[@"poster_path"]]]];
            }
        }
        else
            self.labelSelectedMovieInfo.text = @"Oops! We seem to not have information on that movie in our database. Please click the button below to search for it!";
         [self.imageSelectedMovie setImage: [UIImage imageNamed:@"MovieNotFound"]];
            //[errorAlertView show];
    }];
}

- (IBAction)pressedButtonDeleteMovie:(id)sender
{
    NSMutableArray *userMovieList;
    self.labelMovieTitle.text = @"";
    userMovieList = [[NSMutableArray alloc]init];
    PFUser *currentUser = [PFUser currentUser];
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"MovieList"];
    [query getObjectInBackgroundWithId:self.movieSelected.objectIdString block:^(PFObject *movie, NSError *error)
     {
         // Do something with the returned PFObject in the gameScore variable.
         NSLog(@"Movie Genre %@", movie[@"movieGenre"]);
         NSLog(@"Movie Name %@", movie[@"movieName"]);
         
         [currentUser removeObject:movie forKey:@"UsersMovieList"];
         [currentUser saveInBackground];
     }];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"deleting" object:self.movieSelected];
    
}

- (IBAction)pressedWebViewButton:(id)sender
{
    [self performSegueWithIdentifier:@"toWebView" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toWebView"])  {
        WebViewVC *VC = segue.destinationViewController;
        VC.movieSelected = self.movieSelected;
        
    }
}

@end
