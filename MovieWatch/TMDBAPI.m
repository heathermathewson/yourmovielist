//
//  TMDBAPI.m
//  
//
//  Created by Class on 11/15/15.
//
//

#import "TMDBAPI.h"
#import <JLTMDbClient.h>


@implementation TMDBAPI

-(void)gettingMovies
{
[[JLTMDbClient sharedAPIInstance] GET:kJLTMDbMoviePopular withParameters:nil andResponseBlock:^(id response, NSError *error) {
    if(!error){
        
        NSLog(@"Popular Movies: %@",response);
    }
}];
}

@end
