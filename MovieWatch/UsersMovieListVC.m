//
//  UsersMovieListVC.m
//  MovieWatch
//
//  Created by Class on 10/29/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import "UsersMovieListVC.h"
#import <Parse/Parse.h>
#import "Movies.h"
#import "SelectedMovieVC.h"
#import <Google/Analytics.h>


@interface UsersMovieListVC ()
{
    NSMutableArray *userMovieList;
    NSArray *arrayOfGenres;
 //   NSMutableArray *allViewControllers;
}

@end

@implementation UsersMovieListVC

-(void)viewWillAppear:(BOOL)animated
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Users Movie List"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    

}

-(void)viewDidAppear:(BOOL)animated
{
//    allViewControllers = [[NSMutableArray alloc]init];
//    allViewControllers = [NSMutableArray arrayWithArray: self.navigationController.viewControllers];
//    [allViewControllers removeObjectAtIndex:0];
//    self.navigationController.viewControllers = allViewControllers;
    
    [self.tableViewUsersMovies reloadData];
    arrayOfGenres = [[NSArray alloc]initWithObjects:@"Drama",@"Sci-Fi", @"Action", @"Comedy", @"Romantic Comedy", @"Horror", @"Fantasy", @"Documentary", @"Kids", @"Noir", @"Musical", nil];
 
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userMovieList = [[NSMutableArray alloc]init];
    
    PFUser *currentUser = [PFUser currentUser];
    
    NSArray *arrayOfMovies = [currentUser objectForKey:@"UsersMovieList"];
    
    
    for (PFObject *movieObject in arrayOfMovies)
    {
        PFQuery *query = [PFQuery queryWithClassName:@"MovieList"];
        [query getObjectInBackgroundWithId:movieObject.objectId block:^(PFObject *movie, NSError *error)
         {
             // Do something with the returned PFObject in the gameScore variable.
             NSLog(@"Movie Genre %@", movie[@"movieGenre"]);
             NSLog(@"Movie Genre %@", movie[@"movieName"]);
             
             Movies *newMovieInArray = [Movies movieName:movie[@"movieName"] withGenre:movie[@"movieGenre"] withObjectId:movie.objectId];
             [userMovieList addObject:newMovieInArray];
             [self.tableViewUsersMovies reloadData];
         }];

    }
    
    [[NSNotificationCenter defaultCenter]addObserverForName:@"frequency" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [userMovieList addObject:note.object];
    }];
    [[NSNotificationCenter defaultCenter]addObserverForName:@"deleting" object: nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [userMovieList removeObject:note.object];
    }];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    Movies *movieFromArray = [userMovieList objectAtIndex:indexPath.row];
    cell.textLabel.text = movieFromArray.movieNameString;
    cell.detailTextLabel.text = movieFromArray.movieGenreString;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [userMovieList count];
}

- (IBAction)pressedButtonAddNewMovie:(id)sender
{
    [self performSegueWithIdentifier:@"addNewMovie" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"selectedMovie"])
    {
        SelectedMovieVC *movieBeingMoved = (SelectedMovieVC *)segue.destinationViewController;
        movieBeingMoved.movieSelected = [userMovieList objectAtIndex:self.tableViewUsersMovies.indexPathForSelectedRow.row];
    }
}

//-(NSArray *)tableViewUsersMoviesAtIndexes:(NSIndexSet *)indexes


- (IBAction)pressedRefreshList:(id)sender
{
    [self.tableViewUsersMovies reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"selectedMovie" sender:self];

}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
////    if([Movies ])
////    {
////        return @"Section 0 title";
////    }
////    else if (section == 1){
////        return @"Section 1 title";
////    }
////    return @"title for header";
//    if(section == 0)
//    {
//        return @"Drama";
//    }
//    else if (section == 1){
//        return @"Sci-fi";
//    }
//    else if (section == 2){
//        return @"Action";
//    }
//    else if (section ==3){
//        return @"Comedy";
//    }
//    return @"title for header";
//}
 //   return [arrayOfGenres objectAtIndex:section];
    
   // return arrayOfGenres[section];
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return [arrayOfGenres count];
//}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
//                                 CGRectMake(0, 0, tableView.frame.size.width, 50.0)];
//    sectionHeaderView.backgroundColor = [UIColor cyanColor];
//    
//    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
//                            CGRectMake(15, 15, sectionHeaderView.frame.size.width, 25.0)];
//    
//    headerLabel.backgroundColor = [UIColor clearColor];
//    headerLabel.textAlignment = NSTextAlignmentCenter;
//    [headerLabel setFont:[UIFont fontWithName:@"Verdana" size:20.0]];
//    [sectionHeaderView addSubview:headerLabel];
//    
//    switch (section) {
//        case 0:
//            headerLabel.text = @"Europe";
//            return sectionHeaderView;
//            break;
//        case 1:
//            headerLabel.text = @"Asia";
//            return sectionHeaderView;
//            break;
//        case 2:
//            headerLabel.text = @"South America";
//            return sectionHeaderView;
//            break;
//        default:
//            break;
//    }
//    
//    return sectionHeaderView;
//}


@end
