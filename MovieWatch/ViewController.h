//
//  ViewController.h
//  MovieWatch
//
//  Created by Class on 10/29/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textFieldUserName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UILabel *labelError;

@end

