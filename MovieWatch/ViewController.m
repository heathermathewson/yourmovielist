//
//  ViewController.m
//  MovieWatch
//
//  Created by Class on 10/29/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>
#import "SignUpVC.h"


@interface ViewController ()

@end

@implementation ViewController

NSString *const kUserKey = @"User Token";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
    testObject[@"foo"] = @"bar";
    [testObject saveInBackground];
    
    self.textFieldPassword.secureTextEntry = TRUE;
    PFUser *user = [PFUser currentUser];
    
    if (user)
    {
        [self performSegueWithIdentifier:@"loggedInToMovieList" sender:self];
    }
    
}

- (IBAction)pressedButtonNewUser:(id)sender
{
    [self performSegueWithIdentifier:@"signUpSegue" sender:self];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.textFieldUserName) {
        [self.textFieldPassword becomeFirstResponder];
    }
    else if (textField == self.textFieldPassword)
    {
        [self pressedButtonLogIn:nil];
    }
    return YES;
}

- (IBAction)pressedButtonLogIn:(id)sender
{
    [PFUser logInWithUsernameInBackground:self.textFieldUserName.text password:self.textFieldPassword.text block:^(PFUser * _Nullable user, NSError * _Nullable error)
     {
         if (!error)
         {
             self.labelError.text =@"";
             
             if (user.isAuthenticated)
             {
                 PFUser *user = [PFUser currentUser];
                 user[@"foo"] = @"bar";
                 [user save];
                 [self performSegueWithIdentifier:@"loggedInToMovieList" sender:self];
             }
         }
         else
         {
             self.labelError.text = @"There was an error. Please try again or sign up now.";
         }
         
     }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Segue name"])
    {
        
    }
    
}



@end
