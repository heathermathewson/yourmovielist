//
//  WebViewVC.h
//  MovieWatch
//
//  Created by Class on 11/16/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movies.h"

@interface WebViewVC : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic)Movies *movieSelected;

@end
