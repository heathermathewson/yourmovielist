//
//  WebViewVC.m
//  MovieWatch
//
//  Created by Class on 11/16/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import "WebViewVC.h"
#import "SelectedMovieVC.h"

@interface WebViewVC ()

@end

@implementation WebViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    SelectedMovieVC *movieName = [SelectedMovieVC]
    NSString *fullURL = [@"https://www.google.com/search?q=" stringByAppendingString:[self.movieSelected.movieNameString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];// + @"%@",;
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];

}





@end
