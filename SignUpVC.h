//
//  SignUpVC.h
//  MovieWatch
//
//  Created by Class on 10/29/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpVC : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelErrorMessage;

@property (weak, nonatomic) IBOutlet UITextField *textFieldChooseUsername;
@property (weak, nonatomic) IBOutlet UITextField *textFieldChoosePassword;

@end
