//
//  SignUpVC.m
//  MovieWatch
//
//  Created by Class on 10/29/15.
//  Copyright © 2015 HeatherMathewson. All rights reserved.
//

#import "SignUpVC.h"
#import <Parse/Parse.h>

@interface SignUpVC ()

@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.textFieldChoosePassword.secureTextEntry = TRUE;
}
- (void)signUp
{
    PFUser *user = [PFUser user];
    user.username = self.textFieldChooseUsername.text;
    user.password = self.textFieldChoosePassword.text;
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
     {
         if (!error)
         {
             [self performSegueWithIdentifier:@"newUserToList" sender:self];      }
         else
         {   NSString *errorString = [error userInfo][@"Error"];
             self.labelErrorMessage.text = @"There is already a user with that name. Please choose another.";
             NSLog(@"%@ in signing up", errorString);
             
         }
     }];
}


- (IBAction)pressedCreateAccount:(id)sender
{
    [self signUp];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.textFieldChooseUsername) {
        [self.textFieldChoosePassword becomeFirstResponder];
    }
    else if (textField == self.textFieldChoosePassword)
    {
        [self pressedCreateAccount:nil];
    }
    return YES;
}

@end
